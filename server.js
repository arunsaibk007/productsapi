const express = require('express');
const bodyParser = require('body-parser');
const _ = require('lodash');

const { mongoose } = require('./db/mongoose');
const { Product } = require('./models/product');
const { User } = require('./models/user');
const { authenticate } = require('./middleware/authenticate');

const port = process.env.PORT || 3000;
const app = express();

//middleware to log
app.use((req, res, next) => {
    console.log(`User logged at ${new Date().toString()} : ${req.method} ${req.url} `);
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization, x-auth');
    res.setHeader('Access-Control-Expose-Headers', 'x-auth');
    next();
});

app.use(bodyParser.json());

const baseUrl = '/api/products';

app.get('/', (req, res) => {
    res.send('server is up and running!')
});

//GET
app.get(baseUrl, (req, res) => {
    Product.find().then((prods) => {
        let filterProd = prods.map(prod => {
            return {
                _id: prod._id,
                productName: prod.productName,
                productCode: prod.productCode,
                price: prod.price,
                productRating: prod.productRating,
                imageUrl: prod.imageUrl
            }
        })
        res.send(filterProd);
    }, (e) => {
        res.status(400).send(e);
    });
});

//GET with _id products/_id
app.get(`${baseUrl}/:_id`, (req, res) => {
    let _id = +req.params._id;
    Product.findById(_id).then((prod) => {
        if(!prod){
            return res.status(404).send('No product Found');
        }
        res.send(prod);
    }).catch((e) => {
        res.status(400).send(e)
    });
});

app.post(`${baseUrl}/saveProduct`, (req, res) => {
    let product = new Product({
        _id: req.body._id,
        productName: req.body.productName,
        productCode: req.body.productCode,
        price: req.body.price,
        productRating: req.body.productRating,
        imageUrl: req.body.imageUrl
    });

    product.save().then((doc) => {
        res.send(doc);
    }, (e) => {
        res.status(400).send(e);
    });
});

//DELETE with id products/1
app.delete(`${baseUrl}/:_id`, (req, res) => {
    let _id = +req.params._id;
    Product.findByIdAndRemove(_id).then((prod) => {
        if(!prod){
            return res.status(404).send('No product Found');
        }
        res.send(prod);
    }).catch((e) => {
        res.status(400).send(e)
    });
});

//Update with _id products/1
app.patch(`${baseUrl}/:_id`, (req, res) => {
    let _id = +req.params._id;
    let body = _.pick(req.body,['price','productRating']);
    Product.findByIdAndUpdate(_id,{$set:body},{new:true}).then((prod) => {
        if(!prod){
            return res.status(404).send('No product Found');
        }
        res.send(prod);
    }).catch((e) => {
        res.status(400).send(e)
    });
});


//Users
//POST
app.post(`${baseUrl}/createUser`, (req, res) => {
    let body = _.pick(req.body, ['email','password']);
    let user = new User(body);
    user.save().then(() => {
        return user.generateAuthToken();
    }).then(token=>{
        res.header('x-auth', token).send(user);
    }).catch((e) => {
        res.status(400).send(e);
    });
});



//GET
app.get('/users/me', authenticate ,(req, res) => {
    res.send(req.user);
});

app.listen(port, () => {
    console.log(`server is up and running on poort ${port}`);
});