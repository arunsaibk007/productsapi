let mongoose = require('mongoose');

mongoose.Promise = global.Promise;
process.env.MONGODB_URI  = 'mongodb://testuser:test123@ds121898.mlab.com:21898/productsapp';
mongoose.connect(process.env.MONGODB_URI || 'mongodb://localhost:27017/ProductsApp');

module.exports = {
    mongoose
}