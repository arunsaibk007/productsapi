let mongoose = require('mongoose');

let Product = mongoose.model('Product', {
    _id: {
        type: Number,
        required: true,
        unique: true
    },
    productName : {
        type: String,
        required : true,
        minlength : 1,
        trim : true
    },
    productCode : {
        type: String,
        trim : true
    },
    price : {
        type: Number,
        default : 0
    },
    imageUrl : {
        type: String,
        trim : true,
        default : null
    },
    productRating: {
        type: Number,
        default : 0
    }
});

module.exports = { Product }